// Initialize Quill editor
var toolbarOptions = [[{ 'header': [1, 2, 3, 4, 5, 6, false] }], ['bold', 'italic', 'link', { 'list': 'ordered'}, { 'list': 'bullet' }, 'blockquote']];
var quill = new Quill('#editor', {
  theme: 'snow',
  modules: {
    toolbar: toolbarOptions
  }
});
//Tooltip init
$(function () {
  $('[data-toggle="popover"]').popover()
});
$('.popover-dismiss').popover({
  trigger: 'focus'
});
//Nav
function openNav(){
  $('.overlay').toggle();
  $('.mobile-nav-container').slideToggle(300);
}
$(document).mouseup(function (e)
{
    var container = $(".mobile-nav-container");
    var menu = $('.header-nav');

    if (!container.is(e.target) && container.has(e.target).length === 0 && !menu.is(e.target) && menu.has(e.target).length === 0)
    {
      $(".mobile-nav-container").slideUp();
      $('.overlay').css('display', 'none');
    }
}); 